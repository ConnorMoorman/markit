package utilities.c.markit;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.design.widget.TabLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.util.Xml;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Map;

import utilities.c.markit.dummy.DummyContent;

public class MapActivity extends AppCompatActivity implements MapFragment.OnFragmentInteractionListener,
        LocationHistoryFragment.OnListFragmentInteractionListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;

    // -- Shared Pref Name
    private static final String mPrefsName = "AppSettings";
    private static final String mLatPrefName = "latString";
    private static final String mLonPrefName = "lonString";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);

        // -- Initialize the application context
        AppSingleton.getInstance().init(getApplicationContext());

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        // -- Marker button setup.
        FloatingActionButton markerButton = (FloatingActionButton) findViewById(R.id.markerButton);
        markerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Location useLocation = AppSingleton.getInstance().getLastKnownLocation(getParent());
                LatLng myLocation;

                if(useLocation != null)
                {
                    myLocation = new LatLng(useLocation.getLatitude(), useLocation.getLongitude());
                    MapFragment.getMapFragmentInstance().addMapMarker(myLocation);
                }

                Snackbar.make(view, "Location Saved!", Snackbar.LENGTH_SHORT)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public void onPause() {
        super.onPause();
        MapFragment.getMapFragmentInstance().mapPaused();
    }

    @Override
    public void onStop() {
        super.onStop();

        LatLng lastLatLng = MapFragment.getMapFragmentInstance().getLastMarkerPosition();

        if(lastLatLng != null) {

            try {
                FileOutputStream fos = openFileOutput(mPrefsName, Context.MODE_PRIVATE);

                String storeLocString = String.valueOf(lastLatLng.latitude)
                        + ";"
                        + String.valueOf(String.valueOf(lastLatLng.longitude));
                fos.write(String.valueOf(storeLocString).getBytes());
                fos.close();
            } catch (FileNotFoundException ex) {


            } catch (IOException ex) {

            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        String lat = "";
        String lon = "";

        try {
            FileInputStream fis = openFileInput(mPrefsName);
            BufferedReader br = new BufferedReader( new InputStreamReader(fis));

            StringBuilder sb = new StringBuilder();
            String line;
            while((line = br.readLine()) != null) {
                sb.append(line);
            }

            if(!sb.toString().isEmpty()) {
                String[] parts = sb.toString().split(";");
                lat = parts[0];
                lon = parts[1];
            }

            fis.close();
        } catch (FileNotFoundException ex) {


        } catch (IOException ex) {

        }

        if(!lat.isEmpty() && !lon.isEmpty()) {
            LatLng lastLocation = new LatLng(Double.parseDouble(lat), Double.parseDouble(lon));
            AppSingleton.getInstance().setLastMarkerLocation(lastLocation);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_map, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // -- Take the coordinate selected, focus on it on the Map tab.

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onListFragmentInteraction(DummyContent.DummyItem item) {

    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter {

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            // getItem is called to instantiate the fragment for the given page.
            // Return a PlaceholderFragment (defined as a static inner class below).
            switch (position) {
                case 0:
                    MapFragment map = MapFragment.newInstance();
                    return map;
                case 1:
                    LocationHistoryFragment history = LocationHistoryFragment.newInstance();
                    return history;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 1;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            switch (position) {
                case 0:
                    return "MAP";
                case 1:
                    return "HISTORY";
            }
            return null;
        }
    }
}
