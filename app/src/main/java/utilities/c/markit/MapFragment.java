package utilities.c.markit;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.GroundOverlayOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MapFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MapFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MapFragment extends Fragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener,
        LocationListener {

    private OnFragmentInteractionListener mListener;

    private Context mContext;

    private MapView mapView;

    private static MapFragment mapFragment;

    private static GoogleMap fragmentMap;
    private LatLng lastMarkerPosition;
    private Location lastLocation;

    private AdView mAdView;

    // -- Settings statics
    private final static String prefFileName = "PrefFile";

    public MapFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment MapFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MapFragment newInstance() {
        MapFragment fragment = new MapFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        mapFragment = fragment;
        return fragment;
    }

    public static MapFragment getMapFragmentInstance() {
        if(mapFragment == null) {
            return newInstance();
        }
        return mapFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_map, container, false);

        mapView = (MapView) rootView.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);

        // -- NOTE: This must be called or else the map will not load.
        mapView.onResume();

        MobileAds.initialize(getContext(), "@string/google_api_key");

        // -- Ad handling
        mAdView = (AdView) rootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        // Inflate the layout for this fragment
        return rootView;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;

        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public void onMapReady(GoogleMap map) {

        fragmentMap = map;

        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    PackageManager.PERMISSION_GRANTED);
        }
        else {

            LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

            LatLng myLocation;
            Location useLocation = AppSingleton.getInstance().getLastKnownLocation(getActivity());

            if(useLocation != null)
            {
                myLocation = new LatLng(useLocation.getLatitude(), useLocation.getLongitude());

                map.setMyLocationEnabled(true);

                // -- Move the camera to the specified position.
                map.moveCamera(CameraUpdateFactory.newLatLngZoom(myLocation, 17));
            }
            else {
                AlertDialog.Builder builder;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    builder = new AlertDialog.Builder(getContext(), android.R.style.Theme_Material_Dialog_Alert);
                } else {
                    builder = new AlertDialog.Builder(getContext());
                }
                builder.setTitle("No Location")
                        .setMessage("Are locations services turned off?")
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .setPositiveButton("Enable", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        })
                        .setNegativeButton("Close", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        })
                        .show();
            }

            // -- Check to see if we have a previously saved location.
            if(AppSingleton.getInstance().getLastMarkerLocation() != null) {
                addMapMarker(AppSingleton.getInstance().getLastMarkerLocation());
            }
        }
    }

    public void addMapMarker(LatLng location){
        if(fragmentMap != null) {
            // -- Clear the other markers.
            fragmentMap.clear();

            // -- Set a marker on the map.
            fragmentMap.addMarker(new MarkerOptions()
                    .position(location));
        }

        MapFragment.getMapFragmentInstance().setLastMapMarkerPosition(location);
    }

    public void setLastMapMarkerPosition(LatLng location) {
        this.lastMarkerPosition = location;
    }

    public void navigateToMarker(LatLng location) {
        if(fragmentMap != null) {
            if(lastMarkerPosition != null) {
                // -- Start path to position.
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    PackageManager.PERMISSION_GRANTED);
        }else {
            if(fragmentMap != null) {
                fragmentMap.setMyLocationEnabled(true);
            }
        }
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    public void mapPaused() {
        // -- Save the last location off to retrieve it if the app opens again.

        // -- Stop the location tracking.
        if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(getActivity(), new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    PackageManager.PERMISSION_GRANTED);
        }else {
            fragmentMap.setMyLocationEnabled(false);
        }
    }

    public void updateCurrentLocation(LatLng location) {
        if(location != null) {

            if(ContextCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) !=
                    PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                        PackageManager.PERMISSION_GRANTED);
            }else {
                fragmentMap.setMyLocationEnabled(true);

                // -- Move the camera to the specified position.
                fragmentMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 2));
            }
        }
    }

    public LatLng getLastMarkerPosition() {
        return this.lastMarkerPosition;
    }

    @Override
    public void onLocationChanged(Location location) {
        LocationManager locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);

        LatLng myLocation;
        Location useLocation = AppSingleton.getInstance().getLastKnownLocation(getActivity());

        if(useLocation != null)
        {
            myLocation = new LatLng(useLocation.getLatitude(), useLocation.getLongitude());

            updateCurrentLocation(myLocation);
        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
