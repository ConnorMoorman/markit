package utilities.c.markit.Models;

import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Iterator;

/**
 * Created by Connor on 5/23/2017.
 */

public class LocationObj {
    private double latitude;
    private double longitude;
    String dateCaptured;

    public LocationObj(double lat, double lng) {
        setLongitude(lat);
        setLongitude(lng);
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public LatLng getLatLng() {
        return new LatLng(Double.valueOf(getLatitude()), Double.valueOf(getLongitude()));
    }

    public String getDateCaptured() {
        return dateCaptured;
    }

    public void setDateCaptured(String dateCaptured) {
        this.dateCaptured = dateCaptured;
    }

    public String toJSON() {
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("latitude", getLatitude());
            jsonObject.put("longitude", getLongitude());
            jsonObject.put("date_captured", getDateCaptured());
        } catch (JSONException e) {
            // TODO: Remove print stack trace.
            e.printStackTrace();
        }
        return jsonObject.toString();
    }

    public void fromJSONtoObj(String json) {
        try{
            JSONObject obj = new JSONObject(json);
            for(Iterator<String> iter = obj.keys(); iter.hasNext();) {
                String key = iter.next();
                if(key.compareTo("date_captured") == 0) {
                    String formatted = "";
                    // -- Need to convert date to something we care about.
                    if(obj.get(key).toString().contains(",")) {
                        formatted = obj.get(key).toString().replaceAll(",", "");
                    }
                    setDateCaptured(formatted);
                } else if(key.compareTo("latitude") == 0) {
                    setLatitude(Double.parseDouble(obj.get(key).toString()));
                } else if(key.compareTo("longitude") == 0) {
                    setLongitude(Double.parseDouble(obj.get(key).toString()));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
