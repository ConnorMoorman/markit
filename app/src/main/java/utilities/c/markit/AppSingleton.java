package utilities.c.markit;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import utilities.c.markit.Models.LocationObj;

/**
 * Created by Connor on 5/23/2017.
 */

class AppSingleton {
    private static final AppSingleton ourInstance = new AppSingleton();

    private static List<LocationObj> locationList;

    private static LatLng lastMarkerLocation;

    // -- Context
    private Context mContext;

    static AppSingleton getInstance() {
        return ourInstance;
    }

    private AppSingleton() { }

    public void init(Context context ) {
        if(mContext == null) {
            mContext = context;
        }
    }

    public Context getContext() {
        return mContext;
    }

    // -- Location Services
    public Location getLastKnownLocation(Activity activity) {

        Location bestLocation = null;

        if(ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) !=
                PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[] {  android.Manifest.permission.ACCESS_COARSE_LOCATION  },
                    PackageManager.PERMISSION_GRANTED);
        }
        else {
            LocationManager mLocationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
            List<String> providers = mLocationManager.getProviders(true);

            for (String provider : providers) {
                Location l = mLocationManager.getLastKnownLocation(provider);
                if (l == null) {
                    continue;
                }
                if (bestLocation == null || l.getAccuracy() < bestLocation.getAccuracy()) {
                    // Found best last known location: %s", l);
                    bestLocation = l;
                }
            }
        }
        return bestLocation;
    }

    public static LatLng getLastMarkerLocation() {
        return lastMarkerLocation;
    }

    public static void setLastMarkerLocation(LatLng lastMarkerLocation) {
        AppSingleton.lastMarkerLocation = lastMarkerLocation;
    }
}
